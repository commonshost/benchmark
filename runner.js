require('hard-rejection/register')
const { promisify } = require('util')
const freeTcpPort = require('tcp-free-port')
const { options } = require('./options')
const { directories, files } = require('./setup')
const servers = require('./servers')
const {
  // Wrk,
  H2load,
  H2loadH1
} = require('./clients')
const { sizeInBytes } = require('./sizeInBytes')

const fields = ['name', 'size', 'protocol', 'rps', 'bps']

function append (result) {
  const values = fields.map((field) => result[field])
  console.log(values.join())
}

;(async function () {
  await directories(options)
  await files(options)
  console.log(fields.join())
  const { only, skip } = options
  for (const name of Object.keys(servers)) {
    if (only.length > 0 && !only.includes(name)) continue
    if (skip.length > 0 && skip.includes(name)) continue
    for (const size of options.sizes) {
      const port = await promisify(freeTcpPort)()
      const Server = servers[name]
      const server = new Server(port, options.destination, size)
      await server.start()
      const Client = server.protocol === 'HTTP/1.1' ? /* Wrk */ H2loadH1
        : server.protocol === 'HTTP/2' ? H2load
        : class { constructor () { throw new Error('Unsupported protocol') } }
      const url = `http://localhost:${server.port}${server.pathname}`
      try {
        const result = await new Client(options).start(url)
        append(Object.assign({
          name,
          protocol: server.protocol,
          size: sizeInBytes(size)
        }, result))
      } catch (error) {
        console.error(`${name} @ ${size} failed: ${error.message}`)
      }
      await server.stop()
    }
  }
})()
