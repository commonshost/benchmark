# @http2/benchmark

If you can measure it, you can improve it.

## Overview

This tool is split across several files:

- `options.js` contains a few tunable configuration parameters.
- `setup.js` generates random data files of various sizes, as defined in the configuration, for the web server to publish. This may be pointless as there is no measurable performance difference with just hosting a single file. Live and learn.
- `servers.js` defines webservers that serve the static files in various ways.
- `clients.js` offers task launchers for `wrk` and `h2load` benchmark tools. In my tests `h2load --h1` is ~10% faster than `wrk` though this may be due to differences in throughput & rps reporting.
- `runner.js` coordinates the setup, servers, and clients. It prints errors to stderr and test results as CSV to stdout.
- `source/app.js` The visual report generator is a web app that parses the CSV file in the browser.

## Running the Benchmark

```
npm run clean
npm run start
```

Data is recorded in `results.csv`.

| Column     | Description                                 |
|------------|---------------------------------------------|
| `name`     | Server identifier                           |
| `size`     | Response size in bytes                      |
| `protocol` | Either `HTTP/2` or `HTTP/1.1`               |
| `rps`      | Requests per second as reported by `h2load` |
| `bps`      | Bits per second as reported by `h2load`     |

## Viewing the Report

```
npm run build
npm run server
```

## Prerequisites

- Node.js 8+ with http2 support
- `h2load` a tool that comes with the Nghttp2 suite

## Supported Servers

- Node.js [github:nodejs/http2](https://github.com/nodejs/http2)
- Apache HTTP Server
- Nginx
- H2o
- Nghttpd

## See Also

- [http2server: Benchmarks & Performance Optimisation](https://gitlab.com/http2/server/merge_requests/11)
- [mod_h2, a look at performance](https://icing.github.io/mod_h2/performance.html)
- [H2O Benchmarks](https://h2o.examp1e.net/benchmarks.html)
- [nghttpd vs nginx vs node-spdy vs node-http2](https://github.com/nghttp2/nghttp2/wiki/ServerBenchmarkRoundH210)

## Colophon

Made with ❤️ by Sebastiaan Deckers in 🇸🇬 Singapore.
