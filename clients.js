const { promisify } = require('util')
const { exec } = require('child_process')
const ms = require('ms')
const { sizeInBytes } = require('./sizeInBytes')

const _exec = promisify(exec)

class Client {
  constructor ({ threads, connections, duration }) {
    this.threads = threads
    this.connections = connections
    this.duration = duration
  }
}

// wrk -t 4 -c 100 -d 10s http://localhost:8080/
module.exports.Wrk =
class Wrk extends Client {
  async start (url) {
    const flags = [
      '-c', this.connections,
      '-t', this.threads,
      '-d', this.duration
    ]
    const command = `wrk -c ${flags.join(' ')} "${url}"`
    const { stdout: result } = await _exec(command)
    const pattern = /Requests\/sec:\s+([\d.]+)\nTransfer\/sec:\s+([\d.]+\w+)/
    const [, rps, throughput] = result.match(pattern)

    return {
      rps: parseFloat(rps),
      bps: sizeInBytes(throughput) * 8
    }
  }
}

// h2load -t 4 -c 100 -n 30000 http://localhost:8080/
async function h2load (h1 = false, url, count = 10) {
  const clients = Math.min(count, this.connections)
  const flags = [
    '-c', clients,
    '-t', this.threads,
    '-n', count
  ]
  if (h1 === true) flags.push('--h1')
  const command = `h2load ${flags.join(' ')} "${url}"`
  // const command = `nghttp "${url}"`
  const { stdout: result } = await _exec(command)
  // console.log(result)

  const [, succeeded, failed] = result.match(new RegExp(
    'requests: (?:\\d+) total, (?:\\d+) started, (?:\\d+) done, ' +
    '(\\d+) succeeded, (\\d+) failed, (?:\\d+) errored, (?:\\d+) timeout'
  ))
  if (Number(succeeded) !== count || Number(failed) !== 0) {
    throw new Error('Some requests failed')
  }

  const [, elapsed, rps, throughput] = result.match(new RegExp(
    'finished in ([\\d\\.]+\\w+), ([\\d\\.]+) req\\/s, ([\\d\\.]+\\w+)\\/s'
  ))

  if (ms(elapsed) < ms(this.duration)) {
    const scale = ms(this.duration) / ms(elapsed)
    const padding = 1.25 // Better overshoot a bit than redo almost completely
    return this.start(url, Math.floor(count * scale * padding))
  } else {
    return {
      rps: parseFloat(rps),
      bps: sizeInBytes(throughput) * 8
    }
  }
}

module.exports.H2load =
class H2load extends Client {
  constructor () {
    super(...arguments)
    this.start = h2load.bind(this, false)
  }
}
module.exports.H2loadH1 =
class H2loadH1 extends Client {
  constructor () {
    super(...arguments)
    this.start = h2load.bind(this, true)
  }
}
