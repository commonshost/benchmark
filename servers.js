const { EventEmitter } = require('events')
const { join, basename } = require('path')
const {
  createReadStream,
  readFileSync,
  readdirSync,
  writeSync
} = require('fs')
const { promisify } = require('util')
const { exec, spawn } = require('child_process')
const { fileSync: tmpFile } = require('tmp')
const onEvent = require('p-event')
const waste = require('waste')
const split2 = require('split2')

const _exec = promisify(exec)

const h1 = require('http')
const h2 = require('http2')

class Server extends EventEmitter {
  constructor (port, destination, size) {
    super(...arguments)
    this.port = port
    this.size = size
    this.destination = join(__dirname, destination, size)
    this.pathname = '/'

    this.filepaths = readdirSync(this.destination)
      .map((filename) => join(this.destination, filename))
    this.buffer = readFileSync(this.filepaths[0])
  }
  randomFilepath () {
    const filepaths = this.filepaths
    const index = Math.floor(Math.random() * filepaths.length)
    return filepaths[index]
  }
  async start () {
    throw new Error('Must implement start method')
  }
  async stop () {
    throw new Error('Must implement stop method')
  }
}

class Node extends Server {
  async start () {
    this.server.listen(this.port)
    this.setup()
    await onEvent(this.server, 'listening')
    const { port } = this.server.address()
    this.port = port
  }
  async stop () {
    this.server.close()
    await onEvent(this.server, 'close')
  }
  setup () {
    throw new Error('Must implement setup method')
  }
}

class NodeH1 extends Node {
  constructor () {
    super(...arguments)
    this.protocol = 'HTTP/1.1'
    this.server = h1.createServer()
  }
}

class NodeH2 extends Node {
  constructor () {
    super(...arguments)
    this.protocol = 'HTTP/2'
    this.server = h2.createServer()
  }
}

module.exports.H1FilePipeResponse =
class H1FilePipeResponse extends NodeH1 {
  setup () {
    this.server.on('request', (request, response) => {
      createReadStream(this.randomFilepath()).pipe(response)
    })
  }
}

module.exports.H1BufferWriteResponse =
class H1BufferWriteResponse extends NodeH1 {
  setup () {
    this.server.on('request', (request, response) => {
      response.end(this.buffer)
    })
  }
}

module.exports.H2FilePipeResponse =
class H2FilePipeResponse extends NodeH2 {
  setup () {
    this.server.on('request', (request, response) => {
      createReadStream(this.randomFilepath()).pipe(response)
    })
  }
}

module.exports.H2FilePipeStream =
class H2FilePipeStream extends NodeH2 {
  setup () {
    this.server.on('stream', (stream) => {
      createReadStream(this.randomFilepath()).pipe(stream)
    })
  }
}

module.exports.H2BufferWriteResponse =
class H2BufferWriteResponse extends NodeH2 {
  setup () {
    this.server.on('request', (request, response) => {
      response.end(this.buffer)
    })
  }
}

module.exports.H2BufferWriteStream =
class H2BufferWriteStream extends NodeH2 {
  setup () {
    this.server.on('stream', (stream) => {
      stream.end(this.buffer)
    })
  }
}

module.exports.H2RespondWithFile =
class H2RespondWithFile extends NodeH2 {
  setup () {
    this.server.on('stream', (stream) => {
      stream.respondWithFile(this.randomFilepath())
    })
  }
}

class Daemon extends Server {
  async start () {
    const { name, fd, removeCallback } = tmpFile()
    this.cleanup = removeCallback
    writeSync(fd, this.configuration)
    this.configurationFile = name
    const { stdout: result } = await _exec(this.startCommand())
    if (result !== '') throw new Error(`Failed to start: ${result}`)
    await waste(1000)
  }
  async stop () {
    const { stdout: result } = await _exec(this.stopCommand())
    if (result !== '') throw new Error(`Failed to stop: ${result}`)
    this.cleanup()
    await waste(1000)
  }
}

class Nginx extends Daemon {
  constructor () {
    super(...arguments)
    this.pathname = `/${basename(this.randomFilepath())}`
  }
  startCommand () {
    return `nginx -c "${this.configurationFile}"`
  }
  stopCommand () {
    return `nginx -s stop`
  }
}

module.exports.NginxH1 =
class NginxH1 extends Nginx {
  constructor () {
    super(...arguments)
    this.protocol = 'HTTP/1.1'
    this.configuration = `
      events {}
      http {
        access_log off;
        server {
          listen ${this.port};
          root "${this.destination}";
        }
      }
    `
  }
}

module.exports.NginxH1sendfile =
class NginxH1sendfile extends Nginx {
  constructor () {
    super(...arguments)
    this.protocol = 'HTTP/1.1'
    this.configuration = `
      events {}
      http {
        access_log off;
        sendfile on;
        tcp_nodelay on;
        tcp_nopush on;
        server {
          listen ${this.port};
          root "${this.destination}";
        }
      }
    `
  }
}

module.exports.NginxH2 =
class NginxH2 extends Nginx {
  constructor () {
    super(...arguments)
    this.protocol = 'HTTP/2'
    this.configuration = `
      events {}
      http {
        access_log off;
        server {
          listen ${this.port} http2;
          root "${this.destination}";
        }
      }
    `
  }
}

module.exports.NginxH2sendfile =
class NginxH2 extends Nginx {
  constructor () {
    super(...arguments)
    this.protocol = 'HTTP/2'
    this.configuration = `
      events {}
      http {
        access_log off;
        sendfile on;
        tcp_nodelay on;
        tcp_nopush on;
        server {
          listen ${this.port} http2;
          root "${this.destination}";
        }
      }
    `
  }
}

class Apache extends Daemon {
  constructor () {
    super(...arguments)
    this.pathname = `/${basename(this.randomFilepath())}`
    this.modules = 'lib/httpd/modules'
    this.mpm = 'worker' // event, prefork, worker
  }
  startCommand () {
    return `httpd -f ${this.configurationFile} -k start`
  }
  stopCommand () {
    return `httpd -f ${this.configurationFile} -k stop`
  }
}

module.exports.ApacheH1 =
class ApacheH1 extends Apache {
  constructor () {
    super(...arguments)
    this.protocol = 'HTTP/1.1'
    this.configuration = `
      LoadModule mpm_${this.mpm}_module ${this.modules}/mod_mpm_${this.mpm}.so
      LoadModule unixd_module ${this.modules}/mod_unixd.so
      LoadModule authz_core_module ${this.modules}/mod_authz_core.so
      LoadModule dir_module ${this.modules}/mod_dir.so

      StartServers 1
      MaxRequestWorkers 1
      ServerLimit 1

      ErrorLog off
      DocumentRoot "${this.destination}"

      <Directory "${this.destination}">
        Require all granted
      </Directory>

      Protocols http/1.1
      ServerName localhost
      Listen 0.0.0.0:${this.port}
    `
  }
}

module.exports.ApacheH2 =
class ApacheH2 extends Apache {
  constructor () {
    super(...arguments)
    this.protocol = 'HTTP/2'
    this.configuration = `
      LoadModule mpm_${this.mpm}_module ${this.modules}/mod_mpm_${this.mpm}.so
      LoadModule unixd_module ${this.modules}/mod_unixd.so
      LoadModule authz_core_module ${this.modules}/mod_authz_core.so
      LoadModule dir_module ${this.modules}/mod_dir.so
      LoadModule http2_module ${this.modules}/mod_http2.so

      StartServers 1
      MaxRequestWorkers 1
      ServerLimit 1

      ErrorLog off
      DocumentRoot "${this.destination}"

      <Directory "${this.destination}">
        Require all granted
      </Directory>

      Protocols h2c
      ServerName localhost
      Listen 0.0.0.0:${this.port}
    `
  }
}

class H2o extends Server {
  constructor () {
    super(...arguments)
    this.pathname = `/${basename(this.randomFilepath())}`
    this.configuration = `
      num-threads: 1
      send-server-name: OFF
      file.etag: OFF
      hosts:
        "localhost:${this.port}":
          listen:
            port: ${this.port}
          paths:
            "/":
              file.dir: "${this.destination}"
    `
  }
  start () {
    return new Promise((resolve, reject) => {
      const { name, fd, removeCallback } = tmpFile()
      this.cleanup = removeCallback
      writeSync(fd, this.configuration)
      this.configurationFile = name
      this.child = spawn('h2o', ['-c', this.configurationFile])
      const { stdout, stderr } = this.child
      const lineSplitter = split2()
      const done = /ready to serve requests/
      const fail = /failed/
      function onLine (line) {
        if (done.test(line)) {
          this.removeListener('data', onLine)
          stdout.unpipe(lineSplitter)
          resolve()
        } else if (fail.test(line)) {
          this.removeListener('data', onLine)
          stdout.unpipe(lineSplitter)
          reject(line)
        }
      }
      stderr.pipe(lineSplitter).on('data', onLine)
    })
  }
  async stop () {
    this.cleanup()
    this.child.kill()
    await onEvent(this.child, 'exit')
    await waste(1000)
  }
}

module.exports.H2oH2 =
class H2oH2 extends H2o {
  constructor () {
    super(...arguments)
    this.protocol = 'HTTP/2'
  }
}

module.exports.H2oH1 =
class H2oH1 extends H2o {
  constructor () {
    super(...arguments)
    this.protocol = 'HTTP/1.1'
  }
}

module.exports.Nghttpd =
class Nghttpd extends Server {
  constructor () {
    super(...arguments)
    this.protocol = 'HTTP/2'
    this.pathname = `/${basename(this.randomFilepath())}`
  }
  async start () {
    this.child = spawn(
      'nghttpd', ['--no-tls', '--htdocs', this.destination, this.port]
    )
    await waste(1000)
  }
  async stop () {
    this.child.kill()
    await onEvent(this.child, 'exit')
    await waste(1000)
  }
}
