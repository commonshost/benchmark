const { spawnSync } = require('child_process')

const servers = [
  {
    name: 'nodejs',
    command: 'node',
    args: ['-v'],
    regex: /v([\d.]+)/
  },
  {
    name: 'apache-httpd',
    command: 'httpd',
    args: ['-v'],
    regex: /Server version: Apache\/([\d.]+)/
  },
  {
    name: 'nginx',
    command: 'nginx',
    args: ['-v'],
    regex: /nginx version: nginx\/([\d.]+)/
  },
  {
    name: 'h2o',
    command: 'h2o',
    args: ['-v'],
    regex: /h2o version ([\d.]+)/
  },
  {
    name: 'nghttpd',
    command: 'nghttpd',
    args: ['--version'],
    regex: /nghttpd nghttp2\/([\d.]+)/
  }
]

const versions = []

for (const { name, command, args, regex } of servers) {
  const { stdout, stderr } = spawnSync(command, args)
  const [, version] = regex.exec(stdout + stderr)
  if (version) {
    versions.push({ name, version })
  } else {
    console.warn(`No version found for ${name}: ${stdout}`)
    process.exit(404)
  }
}

console.log(JSON.stringify(versions, null, 2))
