const unitToMultiplier = new Map([
  ['B', 1],
  ['KB', 1000],
  ['KIB', 1024],
  ['MB', 1000 ** 2],
  ['MIB', 1024 ** 2],
  ['GB', 1000 ** 3],
  ['GIB', 1024 ** 3]
])

function sizeInBytes (string) {
  const [, quantity, unit] = string.match(/^(\d+(?:\.\d+)?)(?:\s+)?(\w+)$/)
  return Math.round(
    parseFloat(quantity) *
    unitToMultiplier.get(unit.toUpperCase())
  )
}

module.exports.sizeInBytes = sizeInBytes
