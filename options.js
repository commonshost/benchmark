const { cpus } = require('os')

// Tweak these options to control the behaviour of the load test.

// Path to a directory to store files with random data.
const destination = 'scratch'

// The cumulative size of random data to be generated.
const total = '1 MiB'

// Size of individual data files to be served as response.
const sizes = [
  '512 B',
  '1 KiB',
  '2 KiB',
  '4 KiB',
  '8 KiB',
  '16 KiB',
  '32 KiB',
  '64 KiB',
  '128 KiB',
  '256 KiB',
  '512 KiB',
  '1 MiB'
]

// The number of benchmark client processes to be used.
const threads = cpus().length - 1

// How many client sessions (H2) or sockets (H1) to open simultaneously.
const connections = 100

// The minimum duration to run the benchmark.
const duration = '10s'

// Whitelist of servers to test.
const only = [
  // 'H1FilePipeResponse',
  // 'H1BufferWriteResponse',
  // 'H2FilePipeResponse',
  // 'H2FilePipeStream',
  // 'H2BufferWriteResponse',
  // 'H2BufferWriteStream',
  // 'H2RespondWithFile',
  // 'NginxH1',
  // 'NginxH1sendfile',
  // 'NginxH2',
  // 'NginxH2sendfile',
  // 'ApacheH1',
  // 'ApacheH2',
  // 'H2oH2',
  // 'H2oH1',
  // 'Nghttpd'
]

// Blacklist of servers not to test.
const skip = [
  // 'H1FilePipeResponse',
  // 'H1BufferWriteResponse',
  // 'H2FilePipeResponse',
  // 'H2FilePipeStream',
  // 'H2BufferWriteResponse',
  // 'H2BufferWriteStream',
  // 'H2RespondWithFile',
  // 'NginxH1',
  // 'NginxH1sendfile',
  // 'NginxH2',
  // 'NginxH2sendfile',
  // 'ApacheH1',
  // 'ApacheH2',
  // 'H2oH2',
  // 'H2oH1',
  // 'Nghttpd'
]

module.exports.options = {
  // server
  destination,
  total,
  sizes,

  // client
  threads,
  connections,
  duration,

  // runner
  skip,
  only
}
