const test = require('ava')
const { sizeInBytes } = require('./sizeInBytes')

test('Convert unit syntax to number', (t) => {
  t.is(sizeInBytes('1B'), 1, 'Byte')
  t.is(sizeInBytes('1 B'), 1, 'Whitespace separator')
  t.is(sizeInBytes('1KB'), 1000, 'Kilobyte metric')
  t.is(sizeInBytes('1KiB'), 1024, 'Kilobyte binary')
  t.is(sizeInBytes('1 kib'), 1024, 'Case insensitive')
  t.is(sizeInBytes('2 KiB'), 2 * 1024, 'More than one of unit')
  t.is(sizeInBytes('20 KiB'), 20 * 1024, 'Multiple digits')
  t.is(sizeInBytes('8 MiB'), 8 * 1024 ** 2, 'Megabytes')
  t.is(sizeInBytes('16 GiB'), 16 * 1024 ** 3, 'Gigabytes')
})
