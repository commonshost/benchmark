/* global _, c3 */

import { csvToJson } from './csvToJson.js'
import { abbreviateBytes } from './abbreviateBytes.js'

function millionth (number) {
  const mbps = Math.floor(parseFloat(number) / 1e6)
  return mbps.toLocaleString()
}

function localeInteger (number) {
  return parseInt(number).toLocaleString()
}

document.addEventListener('DOMContentLoaded', async () => {
  const response = await window.fetch('/results.csv', {credentials: 'include'})
  if (!response.ok) throw new Error('Failed to load data')
  const csv = await response.text()
  const json = csvToJson(csv)
  const servers = _.chain(json)
    .groupBy('name')
    .sortBy('size')
    .value()
  const rpsData = {columns: [], axes: {}, type: 'spline'}
  const bpsData = {columns: [], axes: {}, type: 'spline'}
  for (const server of servers) {
    const name = server[0].name
    const rps = [name, ..._.map(server, 'rps')]
    const bps = [name, ..._.map(server, 'bps')]
    rpsData.columns.push(rps)
    bpsData.columns.push(bps)
  }
  const sizes = _.map(servers[0], 'size')
  const axis = {
    x: {
      type: 'category',
      categories: sizes,
      label: { text: 'Response Size', position: 'outer-center' },
      tick: { format: (index) => abbreviateBytes(sizes[index]) }
    },
    y: { label: { position: 'outer-middle' }, min: 0 }
  }
  const chart = {
    size: {
      height: 512
    },
    zoom: {
      enabled: true,
      rescale: true
    }
  }
  c3.generate(Object.assign({
    bindto: '#rps',
    data: rpsData,
    axis: _.defaultsDeep(
      { y: {
        label: { text: 'requests per second' },
        tick: { format: localeInteger }
      } },
      axis
    )
  }, chart))
  c3.generate(Object.assign({
    bindto: '#bps',
    data: bpsData,
    axis: _.defaultsDeep(
      { y: {
        label: { text: 'megabit per second' },
        tick: { format: millionth }
      } },
      axis
    )
  }, chart))
}, {once: true})
