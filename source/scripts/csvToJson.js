export function csvToJson (text) {
  const [header, ...rows] = text.split('\n')
  const headers = header.split(',')
  const results = []
  for (const row of rows) {
    const fields = row.split(',')
    if (fields.length !== headers.length) continue
    const result = {}
    for (let i = 0; i < headers.length; i++) {
      const key = headers[i]
      const value = fields[i]
      const asNumber = parseFloat(value)
      result[key] = isNaN(asNumber) ? value : asNumber
    }
    results.push(result)
  }
  return results
}
