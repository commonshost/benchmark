const sizes = ['B', 'KiB', 'MiB', 'GiB', 'TiB', 'PiB', 'EiB', 'ZiB', 'YiB']

export function abbreviateBytes (bytes, precision = 2) {
  if (bytes === 0) return '0 B'
  const kilo = 1024
  const exponent = Math.floor(Math.log(bytes) / Math.log(kilo))
  const digits = parseFloat((bytes / Math.pow(kilo, exponent)).toFixed(precision))
  const magnitude = sizes[exponent]
  return `${digits} ${magnitude}`
}
