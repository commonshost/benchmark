document.addEventListener('DOMContentLoaded', async () => {
  const response = await window.fetch('/versions.json', { credentials: 'include' })
  if (!response.ok) throw new Error('Failed to load data')
  for (const { name, version } of await response.json()) {
    const element = document.querySelector(`samp.server.version.${name}`)
    element.textContent = `v${version}`
  }
})
