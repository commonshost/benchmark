const { promisify } = require('util')
const {
  // relative,
  join
} = require('path')
const { createWriteStream, mkdirSync } = require('fs')
const { randomBytes } = require('crypto')
const { sizeInBytes } = require('./sizeInBytes')

const _randomBytes = promisify(randomBytes)

module.exports.directories =
async function directories ({ destination, sizes }) {
  function mkdirpSync (path) {
    try {
      mkdirSync(path)
    } catch (error) {
      if (error.code !== 'EEXIST') throw error
    }
  }
  mkdirpSync(join(__dirname, destination))
  for (const size of sizes) {
    mkdirpSync(join(__dirname, destination, size))
  }
}

module.exports.files =
async function files ({ destination, sizes, total }) {
  for (const size of sizes) {
    const sizeBytes = sizeInBytes(size)
    const totalBytes = sizeInBytes(total)
    for (let progress = 0; progress < totalBytes; progress += sizeBytes) {
      const step = (progress / sizeBytes).toFixed(0)
      const filepath = join(__dirname, destination, `${size}/${step}.bin`)
      const data = await _randomBytes(sizeBytes)
      // console.log(
      //   `Writing bytes ${progress + 1}-${progress + sizeBytes} ` +
      //   `out of ${total} of random data ` +
      //   `to "${relative(__dirname, filepath)}"`
      // )
      const file = createWriteStream(filepath)
      file.end(data)
    }
  }
}
